import rp from "request-promise";
import Sequelize from "sequelize";
import Model from "@config/model";
import { InvalidStateException } from "@exceptions/InvalidStateException";
import { TestNotFoundException } from "@exceptions/TestNotFoundException";

export class TestRepository {

    /**
	 * List test
     * @param page
     * @param per_page
     * @returns {TestRepository}
     */
	list (page = 1, per_page = 10) {
		this._page = page;
		this._state = "LIST";
		this._per_page = per_page;
		return this;
	}

    /**
     * Fetch list test for admin
     * @returns {Promise<TestRepository>}
     */
	async admin () {
		this._tests = await Model.TestEntity.findAll({
			raw: true,
			attributes: ["id", "name", "duration", "class_id", "created_by", "createdAt"]
		});
		await this.fillClassesData();
		return this;
	}

    /**
     * Fetch list test for
     * @param classes_id
     * @returns {Promise<TestRepository>}
     */
	async student (classes_id) {
		const Op = Sequelize.Op;
		this._tests = await Model.TestEntity.findAll({ where: {
			class_id: { [Op.in]: classes_id }
		}, raw: true });
		return this;
	}

    /**
     * Fetch list test for teacher
     * @param teacher_id
     * @returns {Promise<TestRepository>}
     */
	async teacher (teacher_id) {
		this._tests = await Model.TestEntity.findAll({ where: {
			created_by: teacher_id
		}, raw: true });
		return this;
	}

    /**
     * Fetch detail test by id
     * @param id
     * @returns {Promise<TestRepository>}
     */
	async detail (id) {
		const test = await Model.TestEntity.findById(id, { raw: true });
		if (test !== null) {
			this._test = test;
			return this;
		} else {
			throw new TestNotFoundException("The data you're looking for couldn't be found!");
		}
	}

    /**
     * Fetch and fill class_name attribute to query result
     * @returns {Promise<TestRepository>}
     */
	async fillClassData () {
		const classes = await rp(`${process.env.CLASS_SERVICE_URI}/class/${this._test.class_id}`, { json: true });
		if (classes !== null) this._test.class_name = classes.data.name;
		return this;
	}

    /**
     * Fetch and fill class_name to query result
     * @returns {Promise<TestRepository | never>}
     */
	async fillClassesData () {
		const classes = await rp(`${process.env.CLASS_SERVICE_URI}/class`, { json: true });
        const promises = [];
        this._tests.forEach(test => {
        	promises.push(
        		classes.data.forEach(cls => {
        			if (cls._id === test.class_id) test.class_name = cls.name;
        		})
        	);
        });
    
        return Promise.all(promises).then(() => {
        	return this;
        });
	}

    /**
     * Return queries result
     * @returns {Promise<*>}
     */
	async get () {
		switch (this._state) {
			case "LIST":
				return this._tests;
			case "DETAIL":
				return this._test;
			default:
				throw new InvalidStateException("Invalid state!");
		}
	}
}