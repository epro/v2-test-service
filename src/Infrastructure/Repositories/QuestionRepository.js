import Model from "@config/model";
import { QuestionNotFoundException } from "@exceptions/QuestionNotFoundException";
import { InvalidStateException } from "@exceptions/InvalidStateException";

export class QuestionRepository {

	_questions;

    /**
	 * List questions
     * @param test_id
     * @param page
     * @param per_page
     * @returns {QuestionRepository}
     */
	list (test_id, page = 1, per_page = 10) {
		this._test_id = test_id;
		this._page = page;
		this._per_page = per_page;
		this._state = "LIST";
		return this;
	}

    /**
	 * Fetch questions list from DB
     * @returns {Promise<QuestionRepository>}
     */
	async fetch () {
		const result = await Model.QuestionEntity.findAndCountAll({
			where: { test_id: this._test_id },
			offset: (this._per_page * (this._page - 1)),
			limit: this._per_page,
			raw: true
		});

		this.questions = {
			total: result.count,
			per_page: this._per_page,
			current_page: this._page,
			last_page: Math.ceil(result.count / this._per_page),
			data: result.row
		};
		return this;
	}

    /**
	 * Detail question
     * @param test_id
     * @param id
     * @returns {Promise<T | never>}
     */
	async detail (test_id, id) {
		return Model.QuestionEntity.findOne({
			where: { id: id, test_id: test_id },
			raw: true
		}).then(question => {
			if (question !== null) {
				this._question = question;
				return this;
			} else {
				throw new QuestionNotFoundException("The question you're looking for couldn't be found!");
			}
		}).catch (error => { throw error });
	}

    /**
	 * Return queries result
     * @returns {Promise<*>}
     */
	async get () {
		switch (this._state) {
			case "LIST":
				return this._questions;
			case "DETAIL":
				return this._question;
			default:
				throw new InvalidStateException("Invalid state!");
		}
	}
}