import { QuestionRepository } from "@repositories/QuestionRepository";
import { CreateQuestionService } from "@questions/Services/CreateQuestionService";
import { UpdateQuestionService } from "@questions/Services/UpdateQuestionService";
import { DeleteQuestionService } from "@questions/Services/DeleteQuestionService";
import { QuestionNotFoundException } from "@exceptions/QuestionNotFoundException";
import { SuccessResponse, NotFoundResponse, InternalServerErrorResponse } from "@responses";

const questions = {};

/**
 * Get list questions by test_id
 * @param req
 * @param res
 * @returns {Promise<*>}
 */
questions.index = async (req, res) => {
	try {
		const result = new QuestionRepository().list(req.params.test_id, req.query.page, req.query.per_page).fetch().then(result => { return result.get() });
		return SuccessResponse(res, "List questions", result);
	} catch (exception) {
		console.log(exception);
		return InternalServerErrorResponse(res, "An error occurred while processing the questions!");
	}
};

/**
 * Get detail question by test_id and question id
 * @param req
 * @param res
 * @returns {Promise<*>}
 */
questions.show = async (req, res) => {
	try {
		const result = new QuestionRepository().detail(req.params.id).then(result => { return result.get() });
		return SuccessResponse(res, "Detail question", result);
	} catch (exception) {
		console.log(exception);
		if (exception instanceof QuestionNotFoundException) {
			return NotFoundResponse(res, exception.message);
		} else {
			return InternalServerErrorResponse(res, "An error occurred while getting question data!");
		}
	}
};

/**
 * Create new question
 * @param req
 * @param res
 * @returns {Promise<*>}
 */
questions.store = async (req, res) => {
	try {
		const result = await new CreateQuestionService(req.params.test_id, req.body).persist();
		return SuccessResponse(res, "Successfully create question", result);
	} catch (exception) {
		console.log(exception);
		return InternalServerErrorResponse(res, "An error occurred while creating question!");
	}
};

/**
 * Update question data by test_id and question id
 * @param req
 * @param res
 * @returns {Promise<*>}
 */
questions.update = async (req, res) => {
	try {
		const result = await new UpdateQuestionService(req.params.test_id, req.params.id, req.body).persist();
		return SuccessResponse(res, "Successfully update question", result);
	} catch (exception) {
		console.log(exception);
		if (exception instanceof QuestionNotFoundException) {
			return NotFoundResponse(res, exception.message);
		} else {
			return InternalServerErrorResponse(res, "An error occurred while updating question!");
		}
	}
};

/**
 * Delete question data by test_id and question id
 * @param req
 * @param res
 * @returns {Promise<*>}
 */
questions.destroy = async (req, res) => {
	try {
		const result = await new DeleteQuestionService(req.params.test_id, req.params.id).persist();
		return SuccessResponse(res, "Successfully delete question data", result);
	} catch (exception) {
		console.log(exception);
		if (exception instanceof QuestionNotFoundException) {
			return NotFoundResponse(res, exception.message);
		} else {
			return InternalServerErrorResponse(res, "An error occurred while updating question!");
		}
	}
};

module.exports = questions;