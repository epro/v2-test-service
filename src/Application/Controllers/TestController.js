import { TestRepository } from "@repositories/TestRepository";
import { CreateTestService } from "@tests/Services/CreateTestService";
import { DeleteTestService } from "@tests/Services/DeleteTestService";
import { UpdateTestService } from "@tests/Services/UpdateTestService";
import { TestNotFoundException } from "@exceptions/TestNotFoundException";
import { SuccessResponse, NotFoundResponse, InternalServerErrorResponse } from "@responses";

const tests = {};

/**
 * Get list tests data
 * @param req
 * @param res
 * @returns {Promise<*>}
 */
tests.index = async (req, res) => {
	try {
		let result, page = req.query.page, per_page = req.query.per_page;
		if (req.headers.role === "ADMIN") result = await new TestRepository().list(page, per_page).admin().then(result => { return result.get() });
		if (req.headers.role === "STUDENT") result = await new TestRepository().list(page, per_page).teacher(req.query.classes_id).then(result => { return result.get() });
		if (req.headers.role === "TEACHER") result = await new TestRepository().list(page, per_page).teacher(req.headers.id).then(result => { return result.get() });
		return SuccessResponse(res, "List tests.", result);
	} catch (exception) {
		console.log(exception);
		return InternalServerErrorResponse(res, exception.message);
	}
};

/**
 * Get detail test by test id
 * @param req
 * @param res
 * @returns {Promise<*>}
 */
tests.show = async (req, res) => {
	try {
		const result = await new TestRepository().detail(req.params.id).get();
		return SuccessResponse(res, "Detail test.", result);
	} catch (exception) {
		console.log(exception);
		if (exception instanceof TestNotFoundException) {
			return NotFoundResponse(res, exception.message);
		} else {
			return InternalServerErrorResponse(res, exception.message);
		}
	}
};

/**
 * Create new test
 * @param req
 * @param res
 * @returns {Promise<*>}
 */
tests.store = async (req, res) => {
	try {
		const result = await new CreateTestService(req.body).persist();
		return SuccessResponse(res, "Successfully create new test.", result);
	} catch (exception) {
		console.log(exception);
		return InternalServerErrorResponse(res, exception.message);
	}
};

/**
 * Update test data by test id
 * @param req
 * @param res
 * @returns {Promise<*>}
 */
tests.update = async (req, res) => {
	try {
		const result = await new UpdateTestService(req.params.id, req.body);
		return SuccessResponse(res, "Successfully update test.", result);
	} catch (exception) {
		console.log(exception);
		if (exception instanceof TestNotFoundException) {
			return NotFoundResponse(res, exception.message);
		} else {
			return InternalServerErrorResponse(res, exception.message);
		}
	}
};

/**
 * Delete test data by test id
 * @param req
 * @param res
 * @returns {Promise<*>}
 */
tests.destroy = async (req, res) => {
	try {
		const result = await new DeleteTestService(req.params.id).persist();
		return SuccessResponse(res, "Successfully delete test.", result);
	} catch (exception) {
		console.log(exception);
		if (exception instanceof TestNotFoundException) {
			return NotFoundResponse(res, exception.message);
		} else {
			return InternalServerErrorResponse(res, exception.message);
		}
	}
};

module.exports = tests;