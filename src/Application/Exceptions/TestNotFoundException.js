export class TestNotFoundException {

    /**
	 * TestNotFoundException constructor
     * @param message
     */
	constructor (message) {
		this.message = message;
	}
}