export class QuestionNotFoundException {

    /**
	 * QuestionNotFoundException constructor
     * @param message
     */
	constructor (message) {
		this.message = message;
	}
}