export class InvalidStateException {

    /**
	 * InvalidStateException constructor
     * @param message
     */
	constructor (message) {
		this.message = message;
	}
}