import SuccessResponse from "./SuccessResponse";
import NotFoundResponse from "./NotFoundResponse";
import UnauthorizedResponse from "./UnauthorizedResponse";
import InternalServerErrorResponse from "./InternalServerErrorResponse";

module.exports = {
	SuccessResponse,
	NotFoundResponse,
	UnauthorizedResponse,
	InternalServerErrorResponse
};