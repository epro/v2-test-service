import TestController from "@controllers/TestController";

const Router = require('restify-router').Router;
const router = new Router();

router.get("/", TestController.index);
router.get("/:id", TestController.show);
router.post("/", TestController.index);
router.put("/:id", TestController.update);
router.del("/:id", TestController.destroy);

module.exports = router;