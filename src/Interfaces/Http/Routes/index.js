const Router = require('restify-router').Router;
const router = new Router();

router.add("/test", require("./Test"));

module.exports = router;