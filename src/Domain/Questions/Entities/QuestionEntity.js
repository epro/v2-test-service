import Sequelize from "sequelize";

/**
 * Define question schema
 * @param sequelize
 * @returns {void|Model|*}
 */
export default sequelize => {
    return sequelize.define("question", {
        id: {
            type: Sequelize.DataTypes.STRING,
            defaultValue: Sequelize.DataTypes.UUIDV4,
            primaryKey: true,
            validate: {
                len: [0, 40]
            }
        },
        assignment_id: {
            type: Sequelize.DataTypes.STRING,
            allowNull: false,
            validate: {
                isUUID: 4
            }
        },
        question: {
            type: Sequelize.DataTypes.TEXT,
            allowNull: false
        },
        option: Sequelize.DataTypes.TEXT,
        answer: {
            type: Sequelize.DataTypes.TEXT,
            allowNull: false
        },
        value: {
            type: Sequelize.DataTypes.INTEGER,
            allowNull: false,
            validate: {
                min: 1,
                max: 100
            }
        },
        created_by: {
            type: Sequelize.DataTypes.STRING,
            allowNull: false,
            validate: {
                len: [0, 30]
            }
        },
        updated_by: {
            type: Sequelize.DataTypes.STRING,
            validate: {
                len: [0, 30]
            }
        },
        deleted_by: {
            type: Sequelize.DataTypes.STRING,
            validate: {
                len: [0, 30]
            }
        }
    }, {
        createdAt: 'createdAt',
        updatedAt: 'updatedAt',
        paranoid: true
    });
};