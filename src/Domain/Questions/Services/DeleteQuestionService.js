import Model from "@config/model";
import { QuestionNotFoundException } from "@exceptions/QuestionNotFoundException";

export class DeleteQuestionService {

    /**
	 * DeleteQuestionService constructor
     * @param test_id
     * @param id
     */
	constructor (test_id, id) {
		this._id = id;
		this._test_id = test_id;
	}

    /**
     * Persist changes to DB
     * @returns {Promise<undefined>}
     */
	async persist () {
		const question = await Model.QuestionEntity.findOne({
			where: {id: this._id, test_id: this._test_id}
		});

		if (question !== null) {
			return question.destroy();
		} else {
			throw new QuestionNotFoundException("The question you're looking for couldn't be found!");
		}
	}
}