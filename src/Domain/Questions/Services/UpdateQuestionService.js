import Model from "@config/model";
import { QuestionNotFoundException } from "@exceptions/QuestionNotFoundException";

export class UpdateQuestionService {

    /**
	 * UpdateQuestionService constructor
     * @param test_id
     * @param id
     * @param data
     */
	constructor (test_id, id, data) {
		this._test_id = test_id;
		this._id = id;
		this._data = data;
	}

    /**
     * Persist changes to DB
     * @returns {Promise<this>}
     */
	async persist () {
		const question = await Model.QuestionEntity.findOne({
			where: {id: this._id, test_id: this._test_id}
		});

		if (question !== null) {
			return question.update(this._data);
		} else {
			throw new QuestionNotFoundException("The question you're looking for couldn't be found!");
		}
	}
}