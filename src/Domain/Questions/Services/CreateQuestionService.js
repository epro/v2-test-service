import Model from "@config/model";

export class CreateQuestionService {

    /**
	 * CreateQuestionService constructor
     * @param test_id
     * @param data
     */
	constructor (test_id, data) {
		this._test_id = test_id;
		this._data = data;
	}

    /**
     * Persist changes to DB
     * @returns {Promise<*>}
     */
	async persist () {
		return Model.QuestionEntity.create({
			test_id: this._test_id,
			...this._data
		});
	}
}