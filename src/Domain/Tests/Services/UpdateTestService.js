import TestEntity from "@tests/Entities/TestEntity";
import { TestNotFoundException } from "@exceptions/TestNotFoundException";

export class UpdateTestService {

    /**
     * UpdateTestService constructor
     * @param id
     * @param data
     */
	constructor (id, data) {
		this._id = id;
		this._data = data;
	}

    /**
     * Persist changes to DB
     * @returns {Promise<*>}
     */
	async persist () {
		const test = await TestEntity.findById(this._id);
		if (test !== null) {
			return test.update(this._data);
		} else {
			throw new TestNotFoundException("The test you're looking for couldn't be found!");
		}
	}
}