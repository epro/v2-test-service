import TestEntity from "@tests/Entities/TestEntity";

export class CreateTestService {

    /**
	 * CreateTestService constructor
     * @param data
     */
	constructor (data) {
		this._data = data;
	}

    /**
	 * Persist changes to DB
     * @returns {Promise<void>}
     */
	async persist () {
		return TestEntity.create(this._data);
	}
}