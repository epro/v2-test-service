import TestEntity from "@tests/Entities/TestEntity";
import { TestNotFoundException } from "@exceptions/TestNotFoundException";

export class DeleteTestService {

    /**
	 * DeleteTestService constructor
     * @param id
     */
	constructor (id) {
		this._id = id;
	}

    /**
	 * Persist changes to DB
     * @returns {Promise<*>}
     */
	async persist () {
		const test = await TestEntity.findById(this._id);
		if (test !== null) {
			return test.destroy();
		} else {
			throw new TestNotFoundException("The test you're looking for couldn't be found!");
		}
	}
}