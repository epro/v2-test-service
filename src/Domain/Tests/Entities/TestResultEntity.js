import Sequelize from "sequelize";

/**
 * Define test result schema
 * @param sequelize
 * @returns {void|Model|*}
 */
export default sequelize => {
    return sequelize.define("test_result", {
        id: {
            type: Sequelize.DataTypes.STRING,
            defaultValue: Sequelize.DataTypes.UUIDV4,
            primaryKey: true,
            validate: {
                isUUID: 4,
                len: [0, 40]
            }
        },
        student_id: {
            type: Sequelize.DataTypes.STRING,
            allowNull: false,
            validate: {
                len: [0, 40]
            }
        },
        test_id: {
            type: Sequelize.DataTypes.STRING,
            allowNull: false,
            validate: {
                len: [0, 40]
            }
        },
        question_id: {
            type: Sequelize.DataTypes.STRING,
            allowNull: false,
            validate: {
                len: [0, 40]
            }
        },
        answer: {
            type: Sequelize.DataTypes.TEXT
        },
        value: {
            type: Sequelize.DataTypes.INTEGER,
            validate: {
                min: 0,
                max: 1000
            }
        },
        created_by: {
            type: Sequelize.DataTypes.STRING,
            allowNull: false,
            validate: {
                len: [0, 40]
            }
        },
        updated_by: {
            type: Sequelize.DataTypes.STRING,
            validate: {
                len: [0, 40]
            }
        },
        deleted_by: {
            type: Sequelize.DataTypes.STRING,
            validate: {
                len: [0, 40]
            }
        }
    }, {
            createdAt: 'createdAt',
            updatedAt: 'updatedAt',
            paranoid: true
        });
};