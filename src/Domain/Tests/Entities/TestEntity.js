import Sequelize from "sequelize";

/**
 * Define test schema
 * @param sequelize
 * @returns {void|Model|*}
 */
export default sequelize => {
    return sequelize.define("test", {
        id: {
            type: Sequelize.DataTypes.STRING,
            defaultValue: Sequelize.DataTypes.UUIDV4,
            primaryKey: true,
            validate: {
                len: [0, 40]
            }
        },
        name: {
            type: Sequelize.DataTypes.STRING,
            allowNull: false,
            validate: {
                len: [0, 255]
            }
        },
        duration: {
            type: Sequelize.DataTypes.INTEGER,
            allowNull: false,
            validate: {
                min: 1
            }
        },
        class_id: {
            type: Sequelize.DataTypes.STRING,
            allowNull: false,
            validate: {
                len: [0, 30]
            }
        },
        created_by: {
            type: Sequelize.DataTypes.STRING,
            allowNull: false,
            validate: {
                len: [0, 30]
            }
        },
        updated_by: {
            type: Sequelize.DataTypes.STRING,
            validate: {
                len: [0, 30]
            }
        },
        deleted_by: {
            type: Sequelize.DataTypes.STRING,
            validate: {
                len: [0, 30]
            }
        },
    }, {
        createdAt: 'createdAt',
        updatedAt: 'updatedAt',
        paranoid: true
    });
};