import restify from "restify";
import Model from "./config/model";
import tunnel from "tunnel-ssh";

/**
 * Create server
 */
const server = restify.createServer({
    name: "test-service",
    ignoreTrailingSlash: true
});

/**
 * Configure restify parser plugins
 */
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.bodyParser());
server.use(restify.plugins.queryParser());

require("dotenv").config();

const config = {
    username: process.env.SSH_USERNAME,
    host: process.env.SSH_HOST,
    privateKey: require("fs").readFileSync(process.env.SSH_PRIVATE_KEY),
    port: process.env.SSH_PORT,
    dstHost: "127.0.0.1",
    dstPort: process.env.DB_PORT,
    localHost: "127.0.0.1",
    localPort: process.env.DB_PORT,
    keepAlive: true
};

if (process.env.APP_ENV === "production") {
    tunnel(config, (error, server) => {
        if (error) {
            throw new Error("An error occurred while tunnelling network!");
        }
    });
}

server.get("/", (req, res) => {
    res.json("SERVER UP!");
});

/**
 * Syncing modelÒ
 */
Model.sequelize.sync({
    force: false
}).then(() => {
    /**
     * Registering routers
     */
    const router = require("@routes");
    router.applyRoutes(server);
    console.log('Schema synced successfully!');
}).catch(err => {
    console.log("An error occurred while syncing schema!");
    console.log(err);
}); 

/**
 * Starting server
 */
server.listen(process.env.PORT || 5000, () => {
    console.log(`Server started at port ${process.env.PORT || 5000}!`);
});

export default server;