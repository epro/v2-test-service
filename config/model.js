import DBConnection from "./database-connection";

const conn = DBConnection;

/**
 * Wrapping the entities
 */
conn.TestEntity = conn.sequelize.import("./../src/Domain/Tests/Entities/TestEntity.js");
conn.QuestionEntity = conn.sequelize.import("./../src/Domain/Questions/Entities/QuestionEntity.js");
conn.TestResultEntity = conn.sequelize.import("./../src/Domain/Tests/Entities/TestResultEntity.js");

/**
 * Detail assignment
 */
conn.QuestionEntity.belongsTo(conn.TestEntity, {
    foreignKey: "assignment_id"
});

/**
 * Assignment has many to questions
 */
conn.TestEntity.hasMany(conn.QuestionEntity, {
    foreignKey: "assignment_id"
});

/**
 * Assignment has many to test result
 */
conn.TestEntity.hasMany(conn.TestResultEntity, {
    foreignKey: "test_id"
});

export default conn;