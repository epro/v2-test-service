import Sequelize from "sequelize";

/**
 * Load env configuration file
 */
require("dotenv").config({
    path: "./.env"
});

/**
 * Attempt to connect to DB
 */
const sequelizeConnection = new Sequelize(process.env.DB_DRIVER + "://" + process.env.DB_USER + ":" + process.env.DB_PASSWORD + "@" + process.env.DB_HOST + "/" + process.env.DB_NAME, {
	operatorsAliases: false
});

sequelizeConnection.authenticate().then(() => {
    console.log('Connection has been established successfully!');
}).catch(err => {
    console.log('Can\'t establish database connection:\n' + err);
});

export default {
    sequelize: sequelizeConnection,
    Sequelize: Sequelize
};